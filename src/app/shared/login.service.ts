import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  private loggedIn: boolean = false;

  doLogin(): void {
    this.loggedIn = true;
  }

  doLogout(): void {
    this.loggedIn = false;
  }

  isAuthenticate(): boolean {
    return this.loggedIn;
  }
}
