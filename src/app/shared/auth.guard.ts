import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {LoginService} from './login.service';
import {Injectable} from '@angular/core';


@Injectable({
  providedIn : 'root'
})
export  class AuthGuard implements CanActivate {

  constructor(private  loginService: LoginService , private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {
    if ( this.loginService.isAuthenticate()) {
      return true;
    }
    this.router.navigate(['']);
    return false;
  }

}
