import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PictureListComponent } from './picture-list/picturelist.component';
import { PicturedetailsComponent } from './picturedetails/picturedetails.component';
import { DashboardModule } from './dashboard/dashboard.module';
import {PictureService} from './shared/picture.service';
import { PictureuploadComponent } from './pictureupload/pictureupload.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import {Route, RouterModule} from '@angular/router';
import {MainComponent} from './dashboard/main/main.component';
import {ProfileComponent} from './dashboard/profile/profile.component';
import { NewsComponent } from './news/news.component';
import { AlertsComponent } from './alerts/alerts.component';
import { TeamComponent } from './team/team.component';
import { MemberComponent } from './member/member.component';
import {LoginService} from './shared/login.service';
import {AuthGuard} from './shared/auth.guard';

const route: Route[]  = [
    {path : '' , component:  MainComponent},
    {path : 'team' , component: TeamComponent},
    {path : 'team/:memberId', component : MemberComponent},
    {path : 'about-us', component: AboutusComponent },
    {path : 'pictures-list' , component : PictureListComponent, canActivate: [AuthGuard]},
    {path : 'profile' , component : ProfileComponent}
    ];



@NgModule({
  declarations: [
    AppComponent,
    PictureListComponent,
    PicturedetailsComponent,
    PictureuploadComponent,
    AboutusComponent,
    NewsComponent,
    AlertsComponent,
    TeamComponent,
    MemberComponent
  ],
  imports: [
    BrowserModule,
    DashboardModule,
    RouterModule.forRoot(route)
  ],
  providers: [{provide: PictureService , useClass: PictureService} , LoginService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
