import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../../shared/login.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private router: Router , private loginService:  LoginService) { }

  ngOnInit() {
  }

  callRoute() {
    this.router.navigate(["alerts"])
  }

  doLogin() {
    console.log('Login in ....')
    this.loginService.doLogin();
    console.log('Logeed !')
  }

  doLogout() {
    console.log('Logging out ....')
    this.loginService.doLogout();
    console.log('Loggedout !')
  }
}
